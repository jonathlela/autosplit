import numpy as np

from typing import Tuple

def compute_resize(
    image_width: int,
    image_height: int,
    display_width: int,
    display_height: int,
) -> Tuple[int, int]:
    if image_width / display_width < image_height / display_height:
        ratio = image_height / display_height
        new_width = round(image_width / ratio)
        new_height = display_height
    else:
        ratio = image_width / display_width
        new_width = display_width
        new_height = round(image_height / ratio)
    return new_width, new_height


def create_black_image(width: int, height: int) -> np.array:
    # Creating a dark square with NUMPY
    black_image = np.zeros((height, width, 3), np.uint8)
    return black_image


def add_bars(
    image: np.array, display_width: int, display_height: int,
) -> np.array:
    image_height, image_width, _ = image.shape

    # Creating a dark square with NUMPY
    image_with_black_bars = create_black_image(display_width, display_height)
    # Getting the centering position
    ax, ay = (
        (display_width - image_width) // 2,
        (display_height - image_height) // 2,
    )
    # Pasting the 'image' in a centering position
    image_with_black_bars[
        ay : image_height + ay, ax : ax + image_width
    ] = image

    return image_with_black_bars
