import os
from enum import Enum, Flag, auto
from functools import reduce
from operator import attrgetter, or_
from typing import List, Optional, Tuple

from parsy import alt, from_enum, regex, seq, string, string_from

# http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.107.8975&rep=rep1&type=pdf

supported_formats = frozenset(
    [
        "png",
        "jpg",
        "jpeg",
        "jpe",
        "jp2",
        "bmp",
        "tiff",
        "tif",
        "dib",
        "webp",
        "pbm",
        "pgm",
        "ppm",
        "sr",
        "ras",
    ]
)


class SplitFlag(Flag):
    NOFLAG = 0
    DUMMY = auto()
    MASK = auto()
    BELOW = auto()
    PAUSE = auto()

    def bitwise_or(*flags):
        return reduce(or_, flags, SplitFlag.NOFLAG)


class SplitEnum(Enum):
    DUMMY = "d"
    MASK = "m"
    BELOW = "b"
    PAUSE = "p"


flags_mark = {
    SplitEnum.DUMMY: SplitFlag.DUMMY,
    SplitEnum.MASK: SplitFlag.MASK,
    SplitEnum.BELOW: SplitFlag.BELOW,
    SplitEnum.PAUSE: SplitFlag.PAUSE,
}


class Options:
    pause: Optional[int] = None
    delay: Optional[int] = None
    threshold: Optional[float] = None
    flags: Optional[SplitFlag] = None
    loops: Optional[int] = None

    def __init__(self, options: List[Tuple]):
        for key, value in options:
            if key == "pause":
                if self.pause is not None:
                    raise Exception("Pause already defined")
                self.pause = value
            if key == "delay":
                if self.delay is not None:
                    raise Exception("Delay already defined")
                self.delay = value
            if key == "threshold":
                if self.threshold is not None:
                    raise Exception("Threshold already defined")
                if value < 0 and value > 1:
                    raise Exception("Threshold may be between 0.0 and 1.0")
                self.threshold = value
            if key == "flags":
                if self.flags is not None:
                    raise Exception("Flags already defined")
                self.flags = SplitFlag.bitwise_or(*value)
            if key == "loops":
                if self.loops is not None:
                    raise Exception("Number of loops already defined")
                self.loops = value
        if self.flags is None:
            self.flags = SplitFlag.NOFLAG

    def __repr__(self):
        return repr(self.__dict__)


class Action:
    filename: str
    extension: str
    pause: Optional[int]
    delay: Optional[int]
    threshold: Optional[float]
    flags: SplitFlag
    loops: Optional[int]

    def __init__(self, filename: str, values: List[Tuple]):
        self.filename = filename
        split = dict(values)
        try:
            self.extension = split["extension"]
        except KeyError as e:
            raise e
        self.pause = split["options"].pause
        self.delay = split["options"].delay
        self.threshold = split["options"].threshold
        self.flags = split["options"].flags
        self.loops = split["options"].loops

    def __repr__(self):
        return repr(self.__dict__)


class Split(Action):
    number: str
    split_name: str

    def __init__(self, filename: str, values: List[Tuple]):
        super(Split, self).__init__(filename, values)
        split = dict(values)
        try:
            self.number = split["split"]["number"]
            self.split_name = split["split"]["split_name"]
        except KeyError as e:
            raise e


class Reset(Action):
    def __init__(self, filename: str, values: List[Tuple]):
        super(Reset, self).__init__(filename, values)


class ActionFactory:
    @staticmethod
    def from_parse(filename: str, values: List[Tuple]) -> Action:
        split = dict(values)
        if "reset" in split:
            return Reset(filename, values)
        elif "split" in split:
            return Split(filename, values)
        else:
            raise NotImplementedError


sep = string("_")
number = regex(r"[0-9]+").tag("number").desc("Number")
split_name = regex(r"[a-zA-Z0-9]+").tag("split_name").desc("Split Name")
pause = regex(r"[0-9]+").map(int).tag("pause").desc("Pause Time (in seconds)")
delay = (
    regex(r"[0-9]+")
    .map(int)
    .tag("delay")
    .desc("Delay Split (in milliseconds)")
)
threshold = (
    regex(r"-?(0|[1-9][0-9]*)([.][0-9]+)?([eE][+-]?[0-9]+)?")
    .map(float)
    .tag("threshold")
    .desc("Threshold")
)
flags = (
    from_enum(SplitEnum)
    .map(lambda x: flags_mark[x])
    .many()
    .tag("flags")
    .desc("Flags")
)
loops = regex(r"[0-9]+").map(int).tag("loops").desc("Number of loops")

extension = string(".") >> string_from(*supported_formats).tag(
    "extension"
).desc("Extension")
pause_arg = string("[") >> pause << string("]")
delay_arg = string("#") >> delay << string("#")
threshold_arg = string("(") >> threshold << string(")")
flags_arg = string("{") >> flags << string("}")
loops_arg = string("@") >> loops << string("@")

options = (
    alt(
        sep >> pause_arg,
        sep >> delay_arg,
        sep >> threshold_arg,
        sep >> flags_arg,
        sep >> loops_arg,
    )
    .many()
    .map(Options)
    .tag("options")
    .desc("Options")
)

split = seq(
    alt(
        seq(number, sep >> split_name).map(dict).tag("split"),
        string("reset").tag("reset"),
    ),
    options,
    extension,
)


def parse_filename(filename: str):
    parse = split.parse(filename)
    s = ActionFactory.from_parse(filename=filename, values=parse)
    return s


def is_supported_image(path: str):
    _, format = os.path.splitext(path)
    if format[1:] in supported_formats:
        return True
    return False


def from_directory(directory: str):
    files = os.listdir(directory)
    reset = None
    splits: List[Split] = list()
    for filename in files:
        if os.path.isfile(os.path.join(directory, filename)):
            if is_supported_image(filename):
                action = parse_filename(filename)
                if type(action) == Split:
                    splits.append(action)
                if type(action) == Reset:
                    reset = action
    splits.sort(key=attrgetter("filename"))
    return splits, reset


def test(filename):
    filename = "001_aueaue_{bp}_[9]_(0.92)_@3299@_#4#.png"
    print(parse_filename(filename=filename))
    filename = "reset_{m}_[9]_(0.92).png"
    print(parse_filename(filename=filename))


if __name__ == "__main__":

    directory = "/home/jonathlela/SMO_Any_Fadeouts"
    splits, reset = from_directory(directory)
    for split in splits:
        print(split)
    print(reset)
