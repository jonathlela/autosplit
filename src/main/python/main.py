from fbs_runtime.application_context.PyQt5 import ApplicationContext
import rx
from rx import operators as ops

# from rx import Observable
from rx.scheduler.mainloop import QtScheduler
from rx.scheduler import ThreadPoolScheduler
from rx.subject import Subject
from PyQt5 import QtCore
from PyQt5.QtWidgets import (
    QMainWindow,
    QHBoxLayout,
    QVBoxLayout,
    QLabel,
    QLineEdit,
    QPushButton,
    QWidget,
    QFileDialog,
    QSpinBox,
    QGridLayout,
    QCheckBox,
)
from PyQt5.QtGui import QPixmap, QImage
from PyQt5.QtCore import pyqtSlot, QCoreApplication
from mss import mss
import numpy as np
import cv2
from pathlib import Path
import sys
import os
from dataclasses import dataclass, asdict
import multiprocessing

# from threading import current_thread
from typing import Tuple

import load as load
from image import create_black_image, compute_resize, add_bars


@dataclass
class ScreenRegion:
    top: int
    left: int
    width: int
    height: int

    def __bool__(self):
        return self.width > 0 and self.height > 0

    def __contains__(self, region):
        return (
            region.top >= self.top
            and region.left >= self.left
            and region.width + region.left <= self.width + self.left
            and region.height + region.top <= self.height + self.top
        )


class MainScreen(QWidget):
    def __init__(
        self,
        splits_folder: Subject,
        capture_zone: Subject,
        capture_rate: Subject,
        should_capture: Subject,
        capture_image: Subject,
        split_image: Subject,
        parent=None,
    ):
        super(MainScreen, self).__init__(parent)

        self.select_folder = SelectFolder(splits_folder=splits_folder)
        self.capture_window = CaptureWindow(
            capture_zone=capture_zone,
            capture_rate=capture_rate,
            should_capture=should_capture,
            capture_image=capture_image,
            split_image=split_image,
        )

        layout = QVBoxLayout()

        layout.addWidget(self.select_folder)
        layout.addWidget(self.capture_window)

        self.setLayout(layout)


class SelectFolder(QWidget):
    def __init__(self, splits_folder: Subject, parent=None):
        super(SelectFolder, self).__init__(parent)

        self.splits_folder = splits_folder
        self.label = QLabel(
            QCoreApplication.translate("SelectFolder", "Split Image Folder:")
        )
        self.folder = QLineEdit()
        self.folder.setReadOnly(True)
        self.open_button = QPushButton(
            QCoreApplication.translate("SelectFolder", "Open")
        )

        layout = QHBoxLayout()

        layout.addWidget(self.label)
        layout.addWidget(self.folder)
        layout.addWidget(self.open_button)

        self.setLayout(layout)

        self.open_button.clicked.connect(self._onOpen)

    @pyqtSlot()
    def _onOpen(self):
        home_dir: str = str(Path.home())
        directory: str = QFileDialog.getExistingDirectory(
            self,
            QCoreApplication.translate("SelectFolder", "Open file"),
            home_dir,
            QFileDialog.ShowDirsOnly,
        )
        if directory:
            self.folder.setText(directory)
            self.splits_folder.on_next(directory)


class CaptureZoneSelector(QWidget):
    def __init__(self, capture_zone: Subject, parent=None):
        super(CaptureZoneSelector, self).__init__(parent)

        self.capture_zone = capture_zone

        max_w: int
        max_h: int

        max_w, max_h = get_max_size()

        self.x_label = QLabel(
            QCoreApplication.translate("CaptureZoneSelector", "left")
        )
        self.x_value = QSpinBox()
        self.x_value.setMaximum(max_w)

        self.y_label = QLabel(
            QCoreApplication.translate("CaptureZoneSelector", "top")
        )
        self.y_value = QSpinBox()
        self.y_value.setMaximum(max_h)

        self.w_label = QLabel(
            QCoreApplication.translate("CaptureZoneSelector", "width")
        )
        self.w_value = QSpinBox()
        self.w_value.setMaximum(max_w)

        self.h_label = QLabel(
            QCoreApplication.translate("CaptureZoneSelector", "height")
        )
        self.h_value = QSpinBox()
        self.h_value.setMaximum(max_h)

        self.keep_ratio = QCheckBox("Keep aspect ratio")

        self.x_value.valueChanged.connect(self._onLeftValueChange)
        self.y_value.valueChanged.connect(self._onTopValueChange)
        self.w_value.valueChanged.connect(self._onWidthValueChange)
        self.h_value.valueChanged.connect(self._onHeightValueChange)

        layout = QGridLayout()
        layout.addWidget(self.x_label, 0, 0)
        layout.addWidget(self.y_label, 0, 1)
        layout.addWidget(self.x_value, 1, 0)
        layout.addWidget(self.y_value, 1, 1)
        layout.addWidget(self.w_label, 2, 0)
        layout.addWidget(self.h_label, 2, 1)
        layout.addWidget(self.w_value, 3, 0)
        layout.addWidget(self.h_value, 3, 1)
        layout.addWidget(self.keep_ratio, 4, 0, 1, 2)

        self.setLayout(layout)

    @pyqtSlot()
    def _onLeftValueChange(self):
        max_w, _ = get_max_size(self.x_value.value(), self.y_value.value())
        self.w_value.setMaximum(max_w - self.x_value.value())
        self.capture_zone.on_next((self.get_capture_region()))

    @pyqtSlot()
    def _onTopValueChange(self):
        _, max_h = get_max_size(self.x_value.value(), self.y_value.value())
        self.h_value.setMaximum(max_h - self.y_value.value())
        self.capture_zone.on_next((self.get_capture_region()))

    @pyqtSlot()
    def _onWidthValueChange(self):
        self.capture_zone.on_next((self.get_capture_region()))

    @pyqtSlot()
    def _onHeightValueChange(self):
        self.capture_zone.on_next((self.get_capture_region()))

    def get_capture_region(self) -> ScreenRegion:
        return ScreenRegion(
            left=self.x_value.value(),
            top=self.y_value.value(),
            width=self.w_value.value(),
            height=self.h_value.value(),
        )


class CaptureRateSelector(QWidget):
    def __init__(
        self, capture_rate, should_capture, capture_image, parent=None
    ):
        super(CaptureRateSelector, self).__init__(parent)

        self.capture_rate = capture_rate
        self.should_capture = should_capture
        capture_image.subscribe(self.onCapture)

        layout = QVBoxLayout()

        should_capture_rate_layout = QHBoxLayout()
        self.should_capture_rate_label = QLabel(
            QCoreApplication.translate("CaptureDisplay", "Should Capture")
        )
        self.should_capture_rate = QCheckBox()
        self.should_capture_rate.stateChanged.connect(self._onCaptureChange)
        should_capture_rate_layout.addWidget(self.should_capture_rate_label)
        should_capture_rate_layout.addWidget(self.should_capture_rate)

        capture_rate_layout = QHBoxLayout()
        self.capture_rate_label = QLabel(
            QCoreApplication.translate("CaptureDisplay", "Capture Rate")
        )
        self.capture_rate_selector = QSpinBox()
        self.capture_rate_selector.valueChanged.connect(
            self._onCaptureRateValueChange
        )
        capture_rate_layout.addWidget(self.capture_rate_label)
        capture_rate_layout.addWidget(self.capture_rate_selector)

        current_capture_rate_layout = QHBoxLayout()
        self.current_capture_rate_label = QLabel(
            QCoreApplication.translate(
                "CaptureDisplay", "Current Capture Rate:"
            )
        )
        self.current_capture_rate_value = QLabel("0")
        current_capture_rate_layout.addWidget(self.current_capture_rate_label)
        current_capture_rate_layout.addWidget(self.current_capture_rate_value)

        layout.addLayout(should_capture_rate_layout)
        layout.addLayout(capture_rate_layout)
        layout.addLayout(current_capture_rate_layout)

        self.setLayout(layout)

    @pyqtSlot()
    def _onCaptureRateValueChange(self):
        self.capture_rate.on_next((self.capture_rate_selector.value()))

    @pyqtSlot()
    def _onCaptureChange(self):
        self.should_capture.on_next((self.should_capture_rate.isChecked()))

    def onCapture(self, mapping):
        _, interval = mapping
        delta: float = 1000000.0 / interval.microseconds
        self.current_capture_rate_value.setText(str(int(round(delta))))


class CaptureDisplay(QWidget):
    def __init__(self, capture_image, parent=None):
        super(CaptureDisplay, self).__init__(parent)

        self.display_size = (400, 300)

        self.should_display = Subject()
        self.display_rate = Subject()

        def interval_mapper(display_rate: float):
            period: float = 1.0 / display_rate
            return rx.interval(period)

        display_interval = self.display_rate.pipe(
            ops.filter(lambda display_rate: display_rate > 0),
            ops.map(interval_mapper),
            ops.switch_latest(),
            ops.time_interval(),
        )

        def display_mapper(to_map):
            should_render, capture_image = to_map
            should_display, _ = to_map
            if should_display:
                return self.display_image(capture_image)

        should_render = rx.combine_latest(
            self.should_display, display_interval
        )

        display = rx.with_latest_from(should_render, capture_image).pipe(
            ops.map(display_mapper), ops.time_interval(),
        )
        display.subscribe(self.onDisplay)

        layout = QVBoxLayout()

        self.capture_label = QLabel()

        show_capture_layout = QHBoxLayout()
        self.show_capture_label = QLabel(
            QCoreApplication.translate("CaptureDisplay", "Show Live Capture")
        )
        self.show_capture = QCheckBox()
        self.show_capture.stateChanged.connect(self._onDisplayChange)
        show_capture_layout.addWidget(self.show_capture_label)
        show_capture_layout.addWidget(self.show_capture)

        display_rate_layout = QHBoxLayout()
        self.display_rate_label = QLabel(
            QCoreApplication.translate("CaptureDisplay", "Display Rate")
        )
        self.display_rate_selector = QSpinBox()
        self.display_rate_selector.valueChanged.connect(
            self._onDisplayRateValueChange
        )
        display_rate_layout.addWidget(self.display_rate_label)
        display_rate_layout.addWidget(self.display_rate_selector)

        current_display_rate_layout = QHBoxLayout()
        self.current_display_rate_label = QLabel(
            QCoreApplication.translate(
                "CaptureDisplay", "Current Display Rate:"
            )
        )
        self.current_display_rate_value = QLabel("0")
        current_display_rate_layout.addWidget(self.current_display_rate_label)
        current_display_rate_layout.addWidget(self.current_display_rate_value)

        layout.addWidget(self.capture_label)
        layout.addLayout(show_capture_layout)
        layout.addLayout(display_rate_layout)
        layout.addLayout(current_display_rate_layout)

        self.setLayout(layout)

    @pyqtSlot()
    def _onDisplayChange(self):
        self.should_display.on_next((self.show_capture.isChecked()))

    @pyqtSlot()
    def _onDisplayRateValueChange(self):
        self.display_rate.on_next(int((self.display_rate_selector.value())))

    def display_image(self, mapping):
        image, _ = mapping
        self.update_capture_screen(image)

    def image_to_pixmap(self, image: np.array) -> QPixmap:
        qimage = QImage(
            image,
            image.shape[1],
            image.shape[0],
            image.shape[1] * 3,
            QImage.Format_RGB888,
        )
        pixmap = QPixmap(qimage)
        return pixmap

    def update_capture_screen(self, image: np.array):
        image_height, image_width, _ = image.shape
        display_width, display_height = self.display_size

        image_width, image_height = compute_resize(
            image_width, image_height, display_width, display_height
        )
        capture: np.ndarray = cv2.resize(image, (image_width, image_height))

        capture: np.ndarray = cv2.cvtColor(capture, cv2.COLOR_BGRA2RGB)

        image_with_black_bars = add_bars(
            capture, display_width, display_height,
        )

        # Convert to set it on the label
        pixmap = self.image_to_pixmap(image_with_black_bars)
        self.capture_label.setPixmap(pixmap)

    def onDisplay(self, mapping):
        _, interval = mapping
        delta: float = 1000000.0 / interval.microseconds
        self.current_display_rate_value.setText(str(int(round(delta))))


class CaptureWindow(QWidget):
    def __init__(
        self,
        capture_zone: Subject,
        capture_rate: Subject,
        should_capture: Subject,
        capture_image: Subject,
        split_image: Subject,
        parent=None,
    ):
        super(CaptureWindow, self).__init__(parent)

        self.capture_zone_selector = CaptureZoneSelector(
            capture_zone=capture_zone
        )
        self.capture_rate_selector = CaptureRateSelector(
            capture_rate=capture_rate,
            should_capture=should_capture,
            capture_image=capture_image,
        )
        self.capture_display = CaptureDisplay(capture_image=capture_image)

        self.split_display = SplitDisplay(split_image=split_image)

        layout = QHBoxLayout()
        capture_layout = QVBoxLayout()
        capture_layout.addWidget(self.capture_zone_selector)
        capture_layout.addWidget(self.capture_rate_selector)
        layout.addLayout(capture_layout)
        layout.addWidget(self.capture_display)
        layout.addWidget(self.split_display)

        self.setLayout(layout)


class SplitDisplay(QWidget):
    def __init__(self, split_image: Subject, parent=None):
        super(SplitDisplay, self).__init__(parent)

        self.display_size = (400, 300)

        self.split_image = split_image
        self.split_image.subscribe(self.onDisplay)

        layout = QVBoxLayout()

        self.split_image_label = QLabel()
        self.display_label = QLabel(
            QCoreApplication.translate("DisplayDisplay", "Split image:")
        )
        self.current_split = QLabel()
        self.set_default_image()

        layout.addWidget(self.split_image_label)
        current_split_info_layout = QHBoxLayout()
        current_split_info_layout.addWidget(self.display_label)
        current_split_info_layout.addWidget(self.current_split)
        layout.addLayout(current_split_info_layout)

        self.setLayout(layout)

    def image_to_pixmap(self, image: np.array) -> QPixmap:
        qimage = QImage(
            image,
            image.shape[1],
            image.shape[0],
            image.shape[1] * 3,
            QImage.Format_RGB888,
        )
        pixmap = QPixmap(qimage)
        return pixmap

    def set_default_image(self):
        display_width, display_height = self.display_size
        default_image = create_black_image(display_width, display_height)
        pixmap = self.image_to_pixmap(default_image)
        self.split_image_label.setPixmap(pixmap)

    def onDisplay(self, split):
        image, action = split

        image_height, image_width, _ = image.shape
        display_width, display_height = self.display_size

        image_width, image_height = compute_resize(
            image_width, image_height, display_width, display_height
        )
        capture: np.ndarray = cv2.resize(image, (image_width, image_height))

        capture: np.ndarray = cv2.cvtColor(capture, cv2.COLOR_BGRA2RGB)

        image_with_black_bars = add_bars(
            capture, display_width, display_height,
        )

        # Convert to set it on the label
        pixmap = self.image_to_pixmap(image_with_black_bars)

        self.split_image_label.setPixmap(pixmap)
        self.current_split.setText(action.filename)


def get_screen() -> ScreenRegion:
    sct = mss()
    return ScreenRegion(**sct.monitors[0])


def get_max_size(top: int = 0, left: int = 0) -> Tuple[int, int]:
    screen = get_screen()
    assert top <= screen.height
    assert left <= screen.width

    max_width: int = screen.width - left
    max_height: int = screen.height - top

    return max_width, max_height


def capture_region(region: ScreenRegion) -> np.ndarray:
    region = asdict(region)
    sct = mss()
    img = np.asarray(sct.grab(region))
    # img = Image.frombytes('RGB', (w,h), sct.grab(region).bgra, "raw", "BGRX")
    return img


class SplitTransition:
    a: int


if __name__ == "__main__":
    appctxt = ApplicationContext()
    scheduler = QtScheduler(QtCore)
    window = QMainWindow()

    optimal_thread_count = multiprocessing.cpu_count()
    pool_scheduler = ThreadPoolScheduler(optimal_thread_count)

    print("Number of CPU : {}".format(optimal_thread_count))

    splits_folder = Subject()
    capture_zone = Subject()
    capture_rate = Subject()
    should_capture = Subject()

    def mapper(capture_rate: float):
        period: float = 1.0 / capture_rate
        return rx.interval(period)

    interval = capture_rate.pipe(
        ops.filter(lambda capture_rate: capture_rate > 0),
        ops.map(mapper),
        ops.switch_latest(),
        ops.time_interval(),
    )

    def capture_mapper(to_map):
        should_capture, capture_zone, interval = to_map
        if should_capture:
            return capture_region(capture_zone)

    capture_image = rx.combine_latest(
        should_capture, capture_zone, interval
    ).pipe(
        #        ops.observe_on(pool_scheduler),
        ops.map(capture_mapper),
        ops.time_interval(),
    )

    def splits_mapper(directory):
        splits, reset = load.from_directory(directory)
        return (directory, splits, reset)

    def split_mapper(to_map):
        directory, splits, _ = to_map
        if len(splits) > 0:
            image_path = os.path.join(directory, splits[0].filename)
            image = cv2.imread(image_path, cv2.IMREAD_UNCHANGED)
            return image, splits[0]

    splits = splits_folder.pipe(ops.map(splits_mapper), ops.map(split_mapper))

    central_widget = MainScreen(
        splits_folder=splits_folder,
        capture_zone=capture_zone,
        capture_rate=capture_rate,
        should_capture=should_capture,
        capture_image=capture_image,
        split_image=splits,
    )
    window.setCentralWidget(central_widget)

    window.show()

    exit_code = appctxt.app.exec_()
    sys.exit(exit_code)
